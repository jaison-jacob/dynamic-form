import {
  Dynamic_Text_fields,
  Advanced_Text_Feild,
} from "../utils/DynamicTextFeilds";
import lodash from "lodash";

let initialState = null;
export const CHANGE_STATE = "CHANGE_STATE";
export const SET_STATE = "SET_STATE";

export const createNewFormRequest = (value) => {
  console.log("calling");
  return async (dispatch, getState) => {
    let currentState = getState().dynamic_form_reducer;

    if (!currentState) {
      currentState = {
        existingUser: {
          userData: value,
          userAction: [],
          id: 0,
          addText: false,
        },
        createdFormData: [],
        dragFormFeildHandle: true,
      };
      dispatch({ type: SET_STATE, value: currentState });
    } else {
      let existingForm = currentState.createdFormData.filter(
        (e) => e.userData.user_name === value.user_name
      );
      if (existingForm.length) {
        currentState.existingUser = {...existingForm[0]};
      } else {
        let template = {
          userData: value,
          userAction: [],
          active: true,
          id: currentState.createdFormData.length,
          addText: false,
        };
        currentState.existingUser = {...template};
        console.log(currentState);
      }
      dispatch({ type: CHANGE_STATE, value: currentState });
    }
  };
};

export const drop = (item) => {
  console.log("calling");
  console.log(item);
  return async (dispatch, getState) => {
    let dynamic = lodash.cloneDeep(Dynamic_Text_fields);
    let advance = lodash.cloneDeep(Advanced_Text_Feild);
    let copyCurrentState = { ...getState().dynamic_form_reducer };
    let copyexistingUser = { ...copyCurrentState.existingUser };
    let userActionChange = copyexistingUser.userAction.map((e, i) => {
      return { ...e, active: false };
    });
    let data = {};
    copyexistingUser.userAction = userActionChange;
    item.status === "Dynamic_Text_fields"
      ? (data = { ...dynamic[item.ind] })
      : (data = { ...advance[item.ind] });
    data.active = true;
    console.log(data);
    copyexistingUser.userAction.push(data);
    copyexistingUser.addText = false;
    copyCurrentState.dragFormFeildHandle = false;
    copyCurrentState.existingUser = copyexistingUser;
    dispatch({ type: CHANGE_STATE, value: copyCurrentState });
  };
};

export const addTextFeild = (index) => {
  console.log("calling");
  return async (dispatch, getState) => {
    let copyCurrentState = { ...getState().dynamic_form_reducer };
    let copyexistingUser = { ...copyCurrentState.existingUser };
    copyexistingUser.userAction[index].select.push({
      value: "",
      checked: false,
    });
    copyexistingUser.userAction[index].addText = true;
    copyCurrentState.existingUser = copyexistingUser;
    dispatch({ type: CHANGE_STATE, value: copyCurrentState });
  };
};

export const addTextFeildOnchange = (value, index, feildkey) => {
  return async (dispatch, getState) => {
    let copyCurrentState = { ...getState().dynamic_form_reducer };
    let copyexistingUser = { ...copyCurrentState.existingUser };
    copyexistingUser.userAction[index].select[feildkey].value = value;
    !value.length
      ? (copyexistingUser.userAction[index].addText = true)
      : (copyexistingUser.userAction[index].addText = false);
    console.log(copyexistingUser.userAction[index].addText);
    copyCurrentState.existingUser = copyexistingUser;
    dispatch({ type: CHANGE_STATE, value: copyCurrentState });
  };
};

export const formHandleChange = () => {
  console.log("calling");
  return async (dispatch, getState) => {
    let copyCurrentState = {
      ...getState().dynamic_form_reducer,
      dragFormFeildHandle: !getState().dynamic_form_reducer.dragFormFeildHandle,
    };
    dispatch({ type: CHANGE_STATE, value: copyCurrentState });
  };
};

export const addTextFeildDelete = (index, feildkey, status) => {
  console.log("calling");
  return async (dispatch, getState) => {
    let copyCurrentState = lodash.cloneDeep(getState().dynamic_form_reducer);
    let copyuser = lodash.cloneDeep(copyCurrentState.createdFormData)
    console.log(copyuser)
    let copyexistingUser =lodash.cloneDeep(copyCurrentState.existingUser.userAction);

    console.log(copyexistingUser);
    if (status === "inner") {
      await copyexistingUser[index].select.splice(feildkey, 1);
      let value = copyexistingUser[index].select[copyexistingUser[index]?.select?.length-1]?.value || "";
    !value.length && !!copyexistingUser[index].select.length
      ? (copyexistingUser[index].addText = true)
      : (copyexistingUser[index].addText = false);
    } else {
      if (
        copyexistingUser[index].active === true &&
        copyexistingUser.length > 1
      ) {
        index === 0
          ? (copyexistingUser[index + 1].active = true)
          : (copyexistingUser[index - 1].active = true);
      }
      await copyexistingUser.splice(index, 1);
      console.log(copyexistingUser);
      !copyexistingUser.length && (copyCurrentState.dragFormFeildHandle = true);
    }
    copyCurrentState.existingUser.userAction = [...copyexistingUser];
    copyCurrentState.createdFormData = [...copyuser]
    console.log(copyCurrentState);
    await dispatch({ type: CHANGE_STATE, value: copyCurrentState });
  };
};

export const textFeildOnchange = (index, value) => {
  console.log("calling");
  return async (dispatch, getState) => {
    let copyCurrentState = { ...getState().dynamic_form_reducer };
    let copyexistingUser = { ...copyCurrentState.existingUser };
    copyexistingUser.userAction[index].value = value;
    copyCurrentState.existingUser = copyexistingUser;
    dispatch({ type: CHANGE_STATE, value: copyCurrentState });
  };
};

export const innerTextFeildDrop = (start, end) => {
  console.log("calling");
  return async (dispatch, getState) => {
    let copyCurrentState = { ...getState().dynamic_form_reducer };
    let copyexistingUser = { ...copyCurrentState.existingUser };
    let copyuserAction = [...copyexistingUser.userAction];
    let temp = copyuserAction[start];
    copyuserAction[start] = copyuserAction[end];
    copyuserAction[end] = temp;
    copyexistingUser.userAction = copyuserAction;
    copyCurrentState.existingUser = copyexistingUser;

    dispatch({ type: CHANGE_STATE, value: copyCurrentState });
  };
};

export const onchangeFocus = (index) => {
  console.log("calling");
  return async (dispatch, getState) => {
    let copyCurrentState = { ...getState().dynamic_form_reducer };
    let copyexistingUser = { ...copyCurrentState.existingUser };
    let userActionChange = copyexistingUser.userAction.map((e, i) => {
      return i === index ? { ...e, active: true } : { ...e, active: false };
    });
    copyexistingUser.userAction = userActionChange;
    copyCurrentState.existingUser = copyexistingUser;
    dispatch({ type: CHANGE_STATE, value: copyCurrentState });
  };
};

export const save = (status) => {
  console.log("calling");
  return async (dispatch, getState) => {
    let copyCurrentState = { ...getState().dynamic_form_reducer };
    let copyexistingUser = { ...copyCurrentState.existingUser };
    if (status === "save") {
      console.log("save cancel");
      let activeFeildChange = copyexistingUser.userAction.map((e, index) => {
        return index === 0 ? { ...e, active: true } : { ...e, active: false };
      });
      copyexistingUser.userAction = activeFeildChange;

      copyCurrentState.createdFormData[copyexistingUser.id] = copyexistingUser;
    }
    // console.log(copyexisting_user)
    copyCurrentState.dragFormFeildHandle = true;
    copyCurrentState.existingUser = {};
    console.log(copyCurrentState);
    dispatch({ type: CHANGE_STATE, value: copyCurrentState });
  };
};

export const edit = (index) => {
  console.log("calling");
  console.log(index);
  return async (dispatch, getState) => {
    let copyCurrentState = { ...getState().dynamic_form_reducer };
    copyCurrentState.existingUser = copyCurrentState.createdFormData[index];
    console.log(copyCurrentState.existingUser);
    dispatch({ type: CHANGE_STATE, value: copyCurrentState });
  };
};

export const deleteCard = (index) => {
  console.log("calling");
  return async (dispatch, getState) => {
    let copyCurrentState = { ...getState().dynamic_form_reducer };
    copyCurrentState.createdFormData.splice(index, 1);
    dispatch({ type: CHANGE_STATE, value: copyCurrentState });
  };
};

export const configuresetting = (event) => {
  console.log("calling");
  return async (dispatch, getState) => {
    let copyCurrentState = { ...getState().dynamic_form_reducer };
    let copyexistingUser = { ...copyCurrentState.existingUser };
    let activeFeildChange = copyexistingUser.userAction.map((e) => {
      return e.active === true ? { ...e, required: event.target.checked } : e;
    });
    copyexistingUser.userAction = activeFeildChange;
    copyCurrentState.existingUser = copyexistingUser;

    dispatch({ type: CHANGE_STATE, value: copyCurrentState });
  };
};

export const mobileSelectOnchange = (index, feildkey, event) => {
  console.log("calling");
  return async (dispatch, getState) => {
    let copyCurrentState = { ...getState().dynamic_form_reducer };
    let copyexistingUser = { ...copyCurrentState.existingUser };
    copyexistingUser.userAction[index].select[feildkey].checked =
      event.target.checked;
    copyCurrentState.existingUser = copyexistingUser;
    dispatch({ type: CHANGE_STATE, value: copyCurrentState });
  };
};

export const mobileRadioOnchange = (index, feildkey, event) => {
  console.log("calling");
  return async (dispatch, getState) => {
    let copyCurrentState = { ...getState().dynamic_form_reducer };
    let copyexistingUser = { ...copyCurrentState.existingUser };
    let radioOnchange = copyexistingUser.userAction[index].select.map(
      (e, i) => {
        return i === feildkey
          ? { ...e, checked: event.target.checked }
          : { ...e, checked: false };
      }
    );

    copyexistingUser.userAction[index].select = radioOnchange;
    copyCurrentState.existingUser = copyexistingUser;

    dispatch({ type: CHANGE_STATE, value: copyCurrentState });
  };
};

export const cancelForm = () => {
  console.log("calling");
  // console.log(callback)
  return async (dispatch, getState) => {
    let copyCurrentState = { ...getState().dynamic_form_reducer };

    copyCurrentState.dragFormFeildHandle = true;
    copyCurrentState.existingUser = {};
    console.log(copyCurrentState);
    dispatch({ type: CHANGE_STATE, value: copyCurrentState });
  };
};

export const dynamic_form_reducer = (state = initialState, action) => {
  console.log("calling");
  switch (action.type) {
    case SET_STATE:
      return { ...action.value };
    case CHANGE_STATE:
      // let data = { ...state } || [];
      // console.log()
      // data = {...action.value};
      return { ...action.value };
    default:
      return state;
  }
};
