import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import AddIcon from '@material-ui/icons/Add';
import Login_Form from '../component/home_page/Login_Form';
import BackspaceIcon from '@material-ui/icons/Backspace';

export default function PopUp() {
  const [open, setOpen] = React.useState(false);
  const handleClickOpen = () => {
    setOpen(true);  
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button variant="outlined" color="primary" onClick={handleClickOpen}>
        <AddIcon/>
      </Button>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
      <DialogActions>
          <Button onClick={handleClose} color="primary">
            <BackspaceIcon/>
          </Button>
        </DialogActions>
        <Login_Form/>
      </Dialog>
    </div>
  );
}
