import React from "react";
import { Button } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import PopUp from "./PopUP";
import Box from "@material-ui/core/Box";
import AppBar from "@material-ui/core/AppBar";
import * as Actions from "../actions/action";



function Header(props) {
  
  const history = useHistory();
  const savefun = async(status) => {
    await props.save(status);
    await history.push("/");
  };
  const cancelfun = async() => {
    await props.cancel();
    await history.push("/");
  };
  const userData = props?.value?.existingUser?.userData
  const usrAction = props.value?.existingUser?.userAction
  return (
    <AppBar position="fixed">
      <Box display="flex" bgcolor="grey.300">
        <Box flexGrow={1} p={2}>
          {!userData ? (
            <div style={{ color:"black",marginTop:10 }}>Dynamic Form</div>
          ) : (
            <Box style={{color:"black",marginTop:10}}>
              {userData.user_name}
            </Box>
          )}
        </Box>
        <Box p={2}>
          {history.location.pathname !== "/form_generation" ? (
            <PopUp />
          ) : (
            <>
              {usrAction?.length !== 0 && (
                <Button variant="contained" color="secondary" onClick={() => savefun("save")}>
                  save
                </Button>
              )}
              <Button
                variant="outlined"
                color="secondary"
                style={{ marginLeft: 10 }}
                onClick={() => cancelfun("cancel")}
              >
                cancel
              </Button>
            </>
          )}
        </Box>
      </Box>
    </AppBar>
  );
}
const mapDispatchToprops = (dispatch) => {
  return bindActionCreators(
    {
      save: Actions.save,
      cancel: Actions.cancelForm
    },
    dispatch
  );
};
const mapStateToProps = (state) => {
  return {
    value: state.dynamic_form_reducer,
  };
};

export default connect(mapStateToProps, mapDispatchToprops)(Header);
