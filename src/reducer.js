import {combineReducers} from 'redux';
import {dynamic_form_reducer} from './actions/action';

export const reducers = combineReducers({
    dynamic_form_reducer:dynamic_form_reducer
})