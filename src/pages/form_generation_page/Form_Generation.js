import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Button } from "@material-ui/core";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import DragFormItem from "../../component/form_generation_page/Drag_form";
import Configure from "../../component/form_generation_page/Configure";
import Header from "../../reusable/Header";
import Mobile_view from "../../component/form_generation_page/Mobile_View";
import Dragable_Area from "../../component/form_generation_page/Dragable_Area";
import * as Actions from "../../actions/action";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  form_feild_container: {
    marginTop: 90,
  },
  contan: {
    width: "100%",
    minHeight: "100vh",
    backgroundColor: "rgb(246,246,246)",
  },
  dragTextFeildContainer: {
    minHeight: "100vh",
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center",
    padding: 20,
  },
  dragTextFeildButtonContainer: {
    width: "90%",
    display: "flex",
    justifyContent: "space-between",
    backgroundColor: "white",
    marginBottom: 20,
  },
  form_generator_table_container: {
    padding: 50,
  },
  Mobile_view_container: {
    marginTop: 70,
    padding: 30,
  },
  paperdesign: {
    backgroundColor: "rgb(246,246,246)",
  },
}));

function Form_Generation(props) {
  const classes = useStyles();

  const [dragable, setdragable] = useState(true);

  const drop = (e) => {
    let s = e.dataTransfer.getData("data");
    console.log(s);
    props.drop(JSON.parse(s));
  };

  const ondragstart = (e, data) => {
    console.log(data);
    e.dataTransfer.setData("data", JSON.stringify(data));
    dragable === false && setdragable(!dragable);
  };
  const ondragstartfeild = (e, data) => {
    e.dataTransfer.setData("data1", JSON.stringify(data));
    dragable && setdragable(!dragable);
  };

  const ondropfeild = (e, a) => {
    let s = e.dataTransfer.getData("data1");
    props.innerTextFeildDrop(JSON.parse(s), a);
  };
  return (
    <Grid container className={classes.root}>
      <Grid item xs={12}>
        {" "}
        <Header />
      </Grid>
      <Grid item xs={3} className={classes.form_feild_container}>
        <Paper className={classes.contan}>
          <div className={classes.dragTextFeildContainer}>
            <div className={classes.dragTextFeildButtonContainer}>
              <Button
                variant={
                  props?.value?.dragFormFeildHandle ? "contained" : "outlined"
                }
                style={
                  props?.value?.dragFormFeildHandle
                    ? {
                        marginLeft: 10,
                        backgroundColor: "rgb(250,106,107)",
                        color: "white",
                      }
                    : {}
                }
                onClick={() => props.formHandleChange()}
                disabled={props?.value?.dragFormFeildHandle}
              >
                feilds
              </Button>
              <Button
                variant={
                  !props?.value?.dragFormFeildHandle ? "contained" : "outlined"
                }
                style={
                  !props?.value?.dragFormFeildHandle
                    ? {
                        marginLeft: 10,
                        backgroundColor: "rgb(250,106,107)",
                        color: "white",
                      }
                    : {}
                }
                onClick={() => props.formHandleChange()}
                disabled={(!props?.value?.dragFormFeildHandle || !props?.value?.existingUser?.userAction?.length || false)}
              >
                configuration
              </Button>
            </div>
            {props?.value?.dragFormFeildHandle ? (
              <>
                <DragFormItem
                  dragFormHandleChange={props.formHandleChange || ""}
                  dragFormFeildHandle={props.value.dragFormFeildHandle || ""}
                  ondragstart={ondragstart}
                  ondropfeild={ondropfeild}
                />
              </>
            ) : (
              <Configure />
            )}
          </div>
        </Paper>
      </Grid>

      <Grid
        item
        xs={6}
        onDragOver={(e) => e.preventDefault()}
        onDrop={(e) => dragable && drop(e)}
        className="form_generator_table_container"
      >
        <Paper className={classes.paperdesign}>
          <Dragable_Area
            dragable={dragable}
            ondragstartfeild={ondragstartfeild || ""}
            ondropfeild={ondropfeild || ""}
          />
        </Paper>
      </Grid>
      <Grid item xs={3} className={classes.Mobile_view_container}>
        <Paper className={classes.paperdesign}>
          <Mobile_view />
        </Paper>
      </Grid>
    </Grid>
  );
}
const mapDispatchToprops = (dispatch) => {
  return bindActionCreators(
    {
      drop: Actions.drop,
      innerTextFeildDrop: Actions.innerTextFeildDrop,
      formHandleChange: Actions.formHandleChange,
    },
    dispatch
  );
};
const mapStateToProps = (state) => {
  return {
    value: state?.dynamic_form_reducer,
  };
};

export default connect(mapStateToProps, mapDispatchToprops)(Form_Generation);
