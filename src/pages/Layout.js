import React from 'react';
import {BrowserRouter as Router,Route,Switch} from 'react-router-dom';
import {routes} from '../router/Routes';

function Layout() {
    return (
        <div>
             <Router>
      <Switch>
        {routes.map((item, index) => (
          <Route
            key={index}
            path={item.path}
            component={item.component}
            exact={item.exact}
          />
        ))}
      </Switch>
    </Router>

        </div>
    )
}

export default Layout;
