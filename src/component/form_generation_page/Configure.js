import React from "react";
import Checkbox from "@material-ui/core/Checkbox";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as Actions from "../../actions/action";

function FormControlLabelPosition(props) {
  return (
    <FormControl component="fieldset">
      <FormGroup aria-label="position" row>
        <FormControlLabel
          value="start"
          control={
            <Checkbox
              className="MuiCheckbox"
              checked={props?.value[0]?.required}
              onChange={(e) => props.configuresetting(e)}
            />
          }
          label="Required"
          labelPlacement="start"
        />
      </FormGroup>
    </FormControl>
  );
}
const mapDispatchToprops = (dispatch) => {
  return bindActionCreators(
    {
      configuresetting: Actions.configuresetting,
    },
    dispatch
  );
};
const mapStateToProps = (state) => {
  let activeFeild = state?.dynamic_form_reducer?.existingUser?.userAction?.filter(
    (e) => {
      return e.active === true;
    }
  );
  console.log(!activeFeild)
  return {
    value: !!activeFeild && [...activeFeild],
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToprops
)(FormControlLabelPosition);
