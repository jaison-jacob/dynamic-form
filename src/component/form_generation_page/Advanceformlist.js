import React from 'react'

function Advanceformlist() {
    return (
        <div>
            {Advanced_Text_Feild.map((e, ind) => (
              <div
                className={classes.textFeildContainer}
                draggable={true}
                onDragStart={(a) => props.ondragstart(a, e)}
                key={ind}
              >
                <div className={classes.dragTextFeildIcon}>{e.icon}</div>
                <div className={classes.dragtextFeildText}>{e.name}</div>
              </div>
            ))}
        </div>
    )
}

export default Advanceformlist
