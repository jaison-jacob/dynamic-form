import React from "react";
import { connect } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import { bindActionCreators } from "redux";
import { useHistory } from "react-router-dom";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import Radio from "@material-ui/core/Radio";
import FormLabel from "@material-ui/core/FormLabel";
import * as Actions from "../../actions/action";
import "../../styles/form_generation_page/table_textfield.scss"

const useStyles = makeStyles((theme) => ({
  checkboxLabel: {
    fontSize: 13,
    wordBreak:"break-all"
},

  formControl: {
    margin: theme.spacing(1),
    minWidth: 170,
    marginTop: 5,
  },
  margintop: {
    marginTop: theme.spacing(1),
    display:"flex",
    flexDirection:"column"
  },
  image_file_container: {
    width: 100,
    marginTop: 10,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  image_file_textfeild: {
    width: 200,
    marginTop: 10,
    margin:"0 auto",
   
  },
  container: {
   
    marginTop: 20,
  },
  textField: {
   width:202
  },
}));

function Mobile_View(props) {
  const classes = useStyles();
  let history = useHistory();
  !props.value.userAction && history.push("/")
  return (
    <div>
      <div className="mobileviewcontainer">
        <div className="mobileviewbox"></div>
        {props?.value?.userAction?.map((e, index) => (
          <div key={index}>
            {(e.name === "Text" ||
              e.name === "Date" ||
              e.name === "Number") && (
             <div style={{margin:"10px 0"}}>
             <label htmlFor="fname" style={{wordBreak:"break-all",fontSize:13}}>{e.value}{
                    (e.required && e.value !== "") && ( <span>*</span>)
                  }</label>
             
              <input type={e.type} id="fname" name="fname" style={{marginTop:10,display:"block",width:200}}/>
              </div>
            )}
            {(e.name === "Multi-Line") && (
              <>
               <label htmlFor="fname" style={{wordBreak:"break-all"}}>{e.value}{
                    (e.required && e.value !== "") && ( <span>*</span>)
                  }</label>
                    <textarea name="Text1" cols="20" rows="2" style={{marginTop:10,display:"block",width:200}}></textarea>
              </>
            )}
            {e.name === "Drop Down" && (
              <div style={{width:225,marginTop:10}}>
                <label htmlFor="fname" style={{wordBreak:"break-all",fontSize:13}}>{e.value}
                {
                    (e.required && e.value !== "") && ( <span>*</span>)
                  }
                </label>
               
  <select style={{width:225,marginTop:10}}>
    {
      e.select.map((a, innerKey) => (
<option value="0" key={innerKey} style={{wordBreak:"break-all",fontSize:13}}>{a.value}</option>
      ))
    }
  </select>
              </div>
            )}
            {e.name === "Radio" && (
              <FormControl
                component="fieldset"
                className={classes.margintop}
                required={e.required}
              >
                <FormLabel
                  component="legend"
                  style={{  color:"black",fontSize: 13,wordBreak:"break-all",marginBottom:10 }}
                >
                  {e.value}
                </FormLabel>
                
                  {e.select.map((a, radioKey) => (
                    <FormControlLabel
                    classes={{
                      label:classes.checkboxLabel
                  }}
                      control={<Radio color="secondary" size="small" style={{marginLeft:10}}/>}
                      checked={a.checked}
                      label={a.value}
                      labelPlacement="end"
                      key={radioKey}
                      onChange={(e) =>
                        props.mobileRadioOnchange(index, radioKey, e)
                      }
                    />
                  ))}
              
              </FormControl>
            )}
            {e.name === "Multi select" && (
              <>
                <FormGroup style={{ marginTop: 10 }}>
                  <FormLabel
                    component="legend"
                    style={{ color:"black",marginBottom:10,fontSize: 13,wordBreak:"break-all" }}
                  >
                    {e.value}
                  </FormLabel>
                  
                    {e.select.map((a, innerKey) => (
                      <FormControlLabel
                      classes={{
                        label:classes.checkboxLabel
                    }}
                      key={innerKey}
                        control={
                          <Checkbox
                            checked={a.checked}
                            style={{fontSize:5,transform:"scale(0.7)"}}
                           
                            name="checkedA"
                            onChange={(e) =>
                              props.mobileSelectOnchange(index, innerKey, e)
                            }
                          />
                        }

                        label={a.value}
                      />
                    ))}
                  
                </FormGroup>
              </>
            )}
            {(e.name === "Image" ||
              e.name === "Video" ||
              e.name === "Audio" ||
              e.name === "File Upload") && (
              <div className={classes.image_file_container}>
                <label
                  htmlFor="img"
                  style={{ fontSize: 13, margintop: 5, color: "black",wordBreak:"break-all" }}
                >
                  {e.value}
                  {
                    (e.required && e.value !== "") && ( <span>*</span>)
                  }
                 
                </label>
                <input
                  type="file"
                  id="img"
                  name="img"
                  accept={e.accept + "/*"}
                  className={classes.image_file_textfeild}
                  required={e.required}
                />
              </div>
            )}
            {e.name === "Date-Time" && (
              <form className={classes.container} noValidate>
                <label
                  htmlFor="img"
                  style={{ width:50,fontSize: 13, margintop: 5, color: "black",wordBreak:"break-all" }}
                >{e.value}</label>
                <TextField
                  id="datetime-local"
                  type="datetime-local"
                  defaultValue="2017-05-24T10:30"
                  className={classes.textField}
                  required={e.required}
                />
              </form>
            )}
          </div>
        ))}
      </div>
    </div>
  );
}
const mapDispatchToprops = (dispatch) => {
  return bindActionCreators(
    {
      mobileSelectOnchange: Actions.mobileSelectOnchange,
      mobileRadioOnchange: Actions.mobileRadioOnchange,
    },
    dispatch
  );
};
const mapStateToProps = (state) => {
  return {
    value: { ...state?.dynamic_form_reducer?.existingUser },
  };
};

export default connect(mapStateToProps, mapDispatchToprops)(Mobile_View);
