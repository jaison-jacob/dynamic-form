import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import FormControl from "@material-ui/core/FormControl";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import AddIcon from "@material-ui/icons/Add";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import OpenWithIcon from "@material-ui/icons/OpenWith";
import { useHistory } from "react-router-dom";
import * as Actions from "../../actions/action";
import "../../styles/form_generation_page/table_textfield.scss";

function Dragable_Area(props) {

  let history = useHistory();
  !props.value.userAction && history.push("/");

  return (
    <div>
      {props.value.userAction?.map((e, i) => {
        return (
          <div
            className="textfeildcontainer"
            draggable={true}
            onDragStart={(v) => props.ondragstartfeild(v, i)}
            onDragOver={(e) => e.preventDefault()}
            onDrop={(e) => !props.dragable && props.ondropfeild(e, i)}
            onClick={() =>props.onchangeFocus(i)}
            style={e.active ? { border: "1px solid black" } : {}}
            key={i}
          >
            <div className="feildcontainer">
              <div className="textfeildicon">{e.icon}</div>
              <div className="textinput">
                <FormControl component="fieldset">
                  <TextField
                    fullWidth={true}
                    placeholder={e.placeholder || ""}
                    id="mui-theme-provider-standard-input"
                    onChange={(textvalue) =>
                      props.textFeildOnchange(i, textvalue.target.value)
                    }
                    autoFocus={false}
                    className="MuiInput-underline MuiInputBase-input "
                    value={e.value || ""}
                  />
                </FormControl>
              </div>
              <Button
                onClick={() => props.addTextFeildDelete(i, null, "outer")}
              >
                <DeleteForeverIcon />
              </Button>
              <OpenWithIcon />
            </div>

            {(e.name === "Radio" ||
              e.name === "Drop Down" ||
              e.name === "Multi select") && (
              <>
                <div className="add_text_container">
                  {e.select.map((addtextfeildvalue, textKey) => (
                    <div className="addinput" key={textKey}>
                      <TextField
                        fullWidth={true}
                        id="mui-theme-provider-standard-input"
                        onChange={(s) =>
                          props.addTextFeildOnchange(s.target.value, i, textKey)
                        }
                        autoFocus={false}
                        className="MuiInput-underline"
                        value={addtextfeildvalue.value || ""}
                      />
                      <Button
                        onClick={() =>
                          props.addTextFeildDelete(i, textKey, "inner")
                        }
                      >
                        <HighlightOffIcon />
                      </Button>
                    </div>
                  ))}
                </div>                        
                <div className="add_btn_container">
                  <Button
                    onClick={() => props.addTextFeild(i)}
                    disabled={e.addText}
                  >
                    <AddIcon />
                    add feild
                  </Button>
                </div>
              </>
            )}
          </div>
        );
      })}
    </div>
  );
}
const mapDispatchToprops = (dispatch) => {
  return bindActionCreators(
    {
      addTextFeild: Actions.addTextFeild,
      addTextFeildOnchange: Actions.addTextFeildOnchange,
      addTextFeildDelete: Actions.addTextFeildDelete,
      textFeildOnchange: Actions.textFeildOnchange,
      // innertextfeielddrop: Actions.addtextfeild_onchange,
      onchangeFocus: Actions.onchangeFocus,
    },
    dispatch
  );
};

const mapStateToProps = (state) => {
  return {
    value: { ...state?.dynamic_form_reducer?.existingUser },
  };
};

export default connect(mapStateToProps, mapDispatchToprops)(Dragable_Area);
