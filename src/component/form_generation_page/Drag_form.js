import React from "react";
import {
  Dynamic_Text_fields,
  Advanced_Text_Feild,
} from "../../utils/DynamicTextFeilds";
import { Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import OpenWithIcon from '@material-ui/icons/OpenWith';
import {connect} from 'react-redux';
import "../../styles/form_generation_page/table_textfield.scss";


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  contan: {
    backgroundColor: "rgb(246,246,246)",
  },
  paper: {
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
  dragTextFeildContainer: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    padding: 20,
  },
  dragTextFeildButtonContainer: {
    width: "100%",
    display: "flex",
    justifyContent: "space-between",
    backgroundColor: "white",
    paddingLeft: 10,
    
    marginBottom: 20,
  
  },
  textFeildContainer: {
    width: "100%",
    height: 40,
    display: "flex",
    marginBottom: 10,
    backgroundColor: "rgb(255,255,255)",
   
  },
  dragTextFeildIcon: {
    width: "30%",
    height: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
     backgroundColor: "rgb(250,106,107)",
    color: "white",
  },
  dragtextFeildText: {
    width: "70%",
    height: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    textAlign: "center",
  },
  advanceListContainer: {
    width:"100%",
    height: "100%",
    
  },
  advanceList: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-evenly",
    alignItems: "center",
  },
  dragadvanceform : {
    width:"250px",
    marginBottom:10,
    backgroundColor: "rgb(255,255,255)",

  }
}));

function Drag_form(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);

  const handleClick = (props) => {
    setOpen(!open);
  };
  console.log(Dynamic_Text_fields);
  return (
    <div className="formscroll">
        {Dynamic_Text_fields.map((feild, index) => (
          <div
            className={classes.textFeildContainer}
            draggable={true}
            onDragStart={(e) => props.ondragstart(e, {ind:index,status:"Dynamic_Text_fields"})}
            key={index}
          >
            <div className={classes.dragTextFeildIcon}>{feild.icon}</div>
            <div className={classes.dragtextFeildText}>{feild.name}</div>
            <div className={classes.dragTextFeildIcon} style={{backgroundColor:"white",color:"black"}}> <OpenWithIcon/></div>
           
          </div>
        ))}
      

      <List
        component="nav"
        aria-labelledby="nested-list-subheader"
        className={classes.advanceListContainer}
      >
        <ListItem
          button
          onClick={handleClick}
          className={classes.dragadvanceform}
        >
          <ListItemText primary="Advanced Feilds" />
          {open ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={open} timeout="auto" unmountOnExit>
          <List component="div" disablePadding className={classes.advanceList}>
            {Advanced_Text_Feild.map((e, index) => (
              <div
                className={classes.textFeildContainer}
                draggable={true}
                onDragStart={(a) => props.ondragstart(a, {ind:index,status:"Advanced_Text_Feild"})}
                key={index}
              >
                <div className={classes.dragTextFeildIcon}>{e.icon}</div>
                <div className={classes.dragtextFeildText}>{e.name}</div>
                <div className={classes.dragTextFeildIcon} style={{backgroundColor:"white",color:"black"}}> <OpenWithIcon/></div>
              </div>
            ))}
          </List>
        </Collapse>
      </List> 
    </div>
  );
}

const mapStateToProps = (state) => { 
  return {
    value: state?.dynamic_form_reducer?.existingUser.userAction,
  };
};

export default connect(mapStateToProps, null)(Drag_form);