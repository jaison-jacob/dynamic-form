import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";
import IconButton from '@material-ui/core/IconButton';
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import ListAltIcon from "@material-ui/icons/ListAlt";
import Cardformname from './Cardformname';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import * as Actions from "../../actions/action";
import "../../styles/home_page/formcard.scss"

const useStyles = makeStyles((theme) => ({
  root: {
      minWidth: 300,
      minHeight:200,
      maxWidth: 345,
      maxHeight:200,
      marginLeft:"20px",
      marginTop:"20px"
  },
 
  avatar: { 
    backgroundColor: "blue",
  },
  nocard_container : {
      width: '90vw',
      height:'50vh',    
      display:"flex",
      justifyContent:"center",
      alignItems:"center"
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));
function FormCard(props) {
  let classes = useStyles();
  let history = useHistory();

 
  const [cardCollection, setCollection] = useState([])

 


  const editfun = (index) => {                                                          
    console.log(index)
    props.edit(index);
    history.push("/form_generation");
  };
  useEffect(() => {
    setCollection(props?.value?.createdFormData)
  },[])
console.log(cardCollection)
  return (
    <div className="form_card_container" >
      {!!props.value && !!props?.value.createdFormData.length ? (
        props?.value.createdFormData.map((e, i) => (
          <Card className={classes.root} key={i}>
            <CardHeader
              avatar={
                <Avatar aria-label="recipe" className={classes.avatar}>
                  <ListAltIcon />
                </Avatar>
              }
              action={
                <>
                <IconButton aria-label="settings" onClick={() => editfun(i)}>
                  <EditIcon />
                </IconButton>
                <IconButton aria-label="settings" onClick={() => props.deleteCard(i)} style={{color:"red"}}>
                <DeleteIcon />
              </IconButton>
              </>
              }
              title={e.userData.user_name}
            />
            <CardContent>
            <Typography gutterBottom variant="h6" component="span">
            Created Feilds
          </Typography>
          <Typography variant="body2" color="textSecondary" component={'span'}>

           <Cardformname userAction={e.userAction}/>
          </Typography>
            </CardContent>
          </Card>
        ))
      ) : (
        <div className={classes.nocard_container}>
          <div>no card create</div>
        </div>
      )
      
      }
      
    </div>
  );
}

const mapDispatchToprops = (dispatch) => {
  return bindActionCreators(
    {
      edit: Actions.edit,
      deleteCard: Actions.deleteCard,
    },
    dispatch
  );
};
const mapStateToProps = (state) => {
  return {
    value: state?.dynamic_form_reducer,
  };
};

export default connect(mapStateToProps, mapDispatchToprops)(FormCard);
