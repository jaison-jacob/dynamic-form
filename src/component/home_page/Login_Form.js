import React from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { makeStyles } from "@material-ui/core/styles";
import { formCreation } from "../../feilds/Form_Creation_Feilds";
import { TextField } from "@material-ui/core";
import { Button } from "@material-ui/core";
import { create_form } from "../../schemas/Createform";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { useHistory } from "react-router-dom";
import * as Actions from "../../actions/action";
import "../../styles/home_page/Form.scss";

const useStyles = makeStyles((theme) => ({
  root: {
    "& label.Mui-focused": {
      color: "black",
      fontWeight: "bold",
    },
    "&.Mui-focused fieldset": {
      borderColor: "white",
    },
    "&.Mui-focused fieldset": {
      borderColor: "white",
    },
    "&.MuiFormHelperText-root": {
      color: "red",
    },
  },
  form_Creation: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 20,
  },
  form_Feild: {
    marginBottom: 20,
  },
  errormsg: {
    color: "red !important",
  },
}));
const initialValue = {
  user_name: "",
  description: "",
};

function Login_Form(props) {
  const classes = useStyles();
  const history = useHistory();
  const onsubmit = async (values) => {
    await props.createNewFormRequest(values);
    await history.push("/form_generation");
  };
  return (
    <div className={classes.form_Creation}>
      <div className="formContainer">
        <h2>Create Form</h2>
        <Formik
          initialValues={initialValue}
          onSubmit={(value, { resetForm }) => {
            onsubmit(value);
            resetForm({});
          }}
          validationSchema={create_form}
        >
          {(props) => (
            <Form className={classes.root}>
              {formCreation.map((feilds, index) => (
                <Field
                  as={TextField}
                  key={index}
                  {...feilds}
                  fullWidth={true}
                  className={classes.form_Feild}
                  helperText={
                    <ErrorMessage
                      name={feilds.name}
                      className="MuiFormHelperText-root"
                    />
                  }
                />
              ))}

              <div className="loginBtn">
                <Button
                  variant="contained"
                  size="large"
                  color="primary"
                  type="submit"
                  fullWidth={true}
                >
                  Submit
                </Button>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
}

const mapDispatchToprops = (dispatch) => {
  return bindActionCreators(
    {
      createNewFormRequest: Actions.createNewFormRequest,
    },
    dispatch
  );
};
const mapStateToProps = (state) => {
  return {
    value: state.dynamic_form_reducer,
  };
};

export default connect(mapStateToProps, mapDispatchToprops)(Login_Form);
