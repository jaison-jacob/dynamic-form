import React from "react";
import { Typography } from '@material-ui/core';
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import "../../styles/home_page/formcard.scss";

function Cardformname(props) {
  console.log(props.user_action);

  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  let copyuserAction = [...props.userAction];
  let mock = copyuserAction.splice(0, 3);
  console.log(mock);
  console.log(copyuserAction);
  return (
    <div>
      {mock.map((e, i) => (
        <Typography
          variant="body2"
          component={'span'}
          style={{ marginTop: 7, fontSize: 13 }}
          key={i}
        >
          {e.name} ,
        </Typography>
      ))}
      <div style={{ margin: "15px auto", float: "right" }}>
        {copyuserAction.length > 0 && (
          <IconButton
            aria-label="more"
            aria-controls="long-menu"
            aria-haspopup="true"
            onClick={handleClick}
          >
            <div style={{ color: "red", fontSize: 20 }}>See More</div>
          </IconButton>
        )}

        <Menu
          id="long-menu"
          anchorEl={anchorEl}
          keepMounted
          open={open}
          onClose={handleClose}
          PaperProps={{
            style: {
              maxHeight: 48 * 4.5,
              width: "20ch",
            },
          }}
        >
          {copyuserAction.map((option, ind) => (
            <MenuItem key={ind} onClick={handleClose}>
              {option.name}
            </MenuItem>
          ))}
        </Menu>
      </div>
    </div>
  );
}

export default Cardformname;
