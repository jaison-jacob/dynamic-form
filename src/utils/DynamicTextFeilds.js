import TitleOutlinedIcon from '@material-ui/icons/TitleOutlined';
import Filter1OutlinedIcon from '@material-ui/icons/Filter1Outlined';
import LibraryAddCheckOutlinedIcon from '@material-ui/icons/LibraryAddCheckOutlined';
import OfflinePinOutlinedIcon from '@material-ui/icons/OfflinePinOutlined';
import ArrowDropDownCircleOutlinedIcon from '@material-ui/icons/ArrowDropDownCircleOutlined';
import DateRangeOutlinedIcon from '@material-ui/icons/DateRangeOutlined';
import ImageSearchOutlinedIcon from '@material-ui/icons/ImageSearchOutlined';
import VideocamOutlinedIcon from '@material-ui/icons/VideocamOutlined';
import MicNoneOutlinedIcon from '@material-ui/icons/MicNoneOutlined';
import AttachFileOutlinedIcon from '@material-ui/icons/AttachFileOutlined';
import EventOutlinedIcon from '@material-ui/icons/EventOutlined';
import CalendarTodayOutlinedIcon from '@material-ui/icons/CalendarTodayOutlined';

export  const  Dynamic_Text_fields = [
    {
        name:'Text',
        icon: <TitleOutlinedIcon/>,
        value:"",
        type:"text",
        active:false,
        required:false,
        placeholder:"Enter the text feild label"
    },
    {
        name:'Number',
        icon: <Filter1OutlinedIcon/>,
        value:"",
        type:"number",
        active:false,
        required:false,
        placeholder:"Enter the Number feild label"
    },
    {
        name:'Multi select',
        icon: <LibraryAddCheckOutlinedIcon/>,
        select:[],
        addtext:false,
        value:"",
        multiline:false,
        active:false,
        required:false,
        placeholder:"Enter the Multi-select feild label"
    },
    {
        name:'Radio',
        icon:<OfflinePinOutlinedIcon/>,
        select:[],
        addtext:false,
        value:"",
        active:false,
        required:false,
        placeholder:"Enter the Radio feild label"
    },
    {
        name:'Drop Down',
        icon:<ArrowDropDownCircleOutlinedIcon/>,
        select:[],
        addtext:false,
        value:"",
        multiline:false,
        active:false,
        required:false,
        placeholder:"Enter the Drop-Down feild label"
    },
    {
        name:'Date',
        icon:<DateRangeOutlinedIcon/>,
        value:"",
        type:"date",
        active:false,
        required:false,
        placeholder:"Enter the Date feild label"
    }
]
export const Advanced_Text_Feild = [
    {
        name:'Image',
        icon: <ImageSearchOutlinedIcon/>,
        active:false,
        accept:"image",
        required:false,
        placeholder:"Enter the Image feild label"
    },
    {
        name:'Video',
        icon: <VideocamOutlinedIcon/>,
        active:false,
        accept:"vidio",
        required:false,
        placeholder:"Enter the Viedio feild label"
    },
    {
        name:'Audio',
        icon: <MicNoneOutlinedIcon/>,
        active:false,
        accept:"audio",
        required:false,
        placeholder:"Enter the Audio feild label"
    },
    {
        name:'File Upload',
        icon:<AttachFileOutlinedIcon/>,
        active:false,
        accept:"",
        required:false,
        placeholder:"Enter the File-upload feild label"
    },
    {
        name:'Date-Time',
        icon:<EventOutlinedIcon/>,
        active:false,
        required:false,
        placeholder:"Enter the date-time feild label"
    },
    {
        name:'Multi-Line',
        icon:<CalendarTodayOutlinedIcon/>,
        active:false,
        multiline:true,
        required:false,
        placeholder:"Enter the Multi-line feild label"
    }
]