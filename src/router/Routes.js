import Home from '../pages/homepage/Home';
import FORM_GENERATION from '../pages/form_generation_page/Form_Generation';
import {dynamic_from_path} from '../router/Path';

export const routes = [
    {
        path: dynamic_from_path.HOME,
        component: Home,
        exact : true
    },
    {
        path: dynamic_from_path.FORM_GENERATION,
        component: FORM_GENERATION,
        exact : true
    }
]