import React from 'react';
import {Provider} from 'react-redux';
import {BrowserRouter  as Router,Route} from 'react-router-dom';
import {store,history} from './store';
import Layout from './pages/Layout';

function App() {
  return (
    <Provider store={store}>
    <Router history={history}>
    <Route path="/" component={Layout} exact={false} />
  </Router>
  </Provider>
  );
}

export default App;
